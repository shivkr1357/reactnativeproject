import React, { useState } from 'react';
import {
   StyleSheet,
   Text,
   View,
   ScrollView,
   TextInput,
   Button,
   FlatList,
} from 'react-native';
import TodoItem from './components/TodoItem';
import TodoInput from './components/TodoInput';

export default function App() {
   const [todoList, setTodoList] = useState([]);

   const todoListHandler = (newItem) => {
      setTodoList((todoList) => [
         ...todoList,
         { id: Math.random().toString(), value: newItem },
      ]);
   };
   const removeItem = (item) => {
      setTodoList((todoList) => {
         return todoList.filter((previousItem) => previousItem.id !== item);
      });
   };
   return (
      <View style={styles.container}>
         <TodoInput onAddItem={todoListHandler} />

         <FlatList
            data={todoList}
            renderItem={(todoItem) => (
               <TodoItem
                  id={todoItem.item.id}
                  onDelete={removeItem}
                  title={todoItem.item.value}
               />
            )}
         />
      </View>
   );
}

const styles = StyleSheet.create({
   container: {
      padding: 50,
      width: '100%',
   },
});
