import React, { useState } from 'react';
import { View, TextInput, Button, StyleSheet } from 'react-native';

const TodoInput = (props) => {
   const [todoItem, setTodoItem] = useState('');

   const handleChange = (item) => {
      setTodoItem(item);
   };
   return (
      <View style={styles.inputTextView}>
         <TextInput
            style={styles.inputText}
            placeholder='Enter Any todo item'
            onChangeText={handleChange}
            value={todoItem}
         />
         <Button
            title='Add'
            style={styles.button}
            onPress={props.onAddItem.bind(this, todoItem)}
         />
      </View>
   );
};

const styles = StyleSheet.create({
   inputTextView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '90%',
      padding: 10,
   },
   inputText: {
      padding: 10,
      width: '80%',
      borderColor: 'black',
      backgroundColor: '#ccc',
   },
   button: {
      padding: 10,
      width: '80%',
   },
});

export default TodoInput;
