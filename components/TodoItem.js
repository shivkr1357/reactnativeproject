import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const TodoItem = (props) => {
   return (
      <TouchableOpacity onPress={props.onDelete.bind(this, props.id)}>
         <View style={styles.todoList}>
            <Text>{props.title}</Text>
         </View>
      </TouchableOpacity>
   );
};

const styles = StyleSheet.create({
   todoList: {
      padding: 10,
      marginVertical: 10,
      borderColor: '#fff',
      borderWidth: 1,
      backgroundColor: '#ccc',
   },
});

export default TodoItem;
